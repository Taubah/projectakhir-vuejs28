Project VueJS Batch 28
Deskripsi
Repositori ini adalah repositiori yang berisi tugas proyek akhir untuk bootcamp online VueJS (Batch 28) di SanberCode. Proyek ini menggunakan VueJS 2, Vue CLI, Vuetify, dan Vue Router.

Link
Berikut ini adalah link untuk video demo aplikasi https://youtu.be/z37B6NTAiVo

Berikut ini adalah link untuk screenshot aplikasi https://drive.google.com/drive/folders/1ROabtIw-blzxJ3X5X7DZAVPa5bPk30w9?usp=sharing
Prasyarat
NPM
Instalasi
# Instal dependencies NPM
npm install
Menjalankan App
# Menjalankan app dalam development build
npm run serve

# Menjalankan app dalam production build
npm run build

dibuat oleh Taubah fadja
